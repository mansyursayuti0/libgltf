# Change Log

## 0.1.7 (2020/10/15)

* Supports the `KHR_lights_punctual`
* Compile by github action

## 0.1.6

* Fix some bugs

## 0.1.5

* Fix some bugs

## 0.1.4 (2018/11/5)

* Support Android and iOS platforms

## 0.0.1 (2017/12/5)

### Changes

* Integrate more applications to check the project
* Depoly the output files when build tags
